import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AreaMemoria {
  String nome = "";
  int addrInicio = 0;
  int tamanho = 0;
  Color cor = (Colors.white);

  AreaMemoria(this.nome, this.addrInicio, this.tamanho, this.cor);

  Map<String, dynamic> toJson() {
    int index = this.cor.toString().indexOf('Color(0') + 6;
    String retornoCor = this.cor.toString().substring(index, index + 10);
    //print(index);
    //print(this.cor.toString());
    //print(retornoCor);
    return {
      "\"nome\"": "\"${this.nome}\"",
      "\"addrInicio\"": "\"${this.addrInicio}\"",
      "\"tamanho\"": "\"${this.tamanho}\"",
      "\"cor\"": "\"${retornoCor}\""
    };
  }

  factory AreaMemoria.fromJson(Map<String, dynamic> json) {
    return AreaMemoria(
      json['nome'],
      int.parse(json['addrInicio']),
      int.parse(json['tamanho']),
      new Color(int.parse(json['cor'])),
    );
  }
}
