import 'dart:convert';
import 'dart:html' as html;
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'AreaMemoria.dart';
import 'Ponto.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:share/share.dart';
import 'package:flutter/rendering.dart';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter/foundation.dart' show kIsWeb;

bool pressphone = false;
bool pressphone_old = true;
bool editar_visivel = false;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mapa de Memória',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Mapa de Memória'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //int _counter = 0;
  static GlobalKey previewContainer = new GlobalKey();

  String diretorio = "";
  String diretorio_json = "";

  List<AreaMemoria> listaMemorias = List();
  List<Ponto> listaPontos = List();

  FilePickerCross filePickerCross = FilePickerCross(null);

  String _fileString = '';
  Set<String> lastFiles;
  FileQuotaCross quota = FileQuotaCross(quota: 0, usage: 0);

  int tamanhoMapa = 64;

  Color currentColor = Colors.limeAccent;

  void changeColor(Color color) => setState(() => currentColor = color);

  void novaMemoria(AreaMemoria mem_old) {
    AreaMemoria memoria = new AreaMemoria(
        "",
        0,
        0,
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
            .withOpacity(1.0));
    bool aux = false;
    if (mem_old != null) {
      memoria = mem_old;
      aux = true;
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Positioned(
                  right: -40.0,
                  top: -40.0,
                  child: InkResponse(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: CircleAvatar(
                      child: Icon(Icons.close),
                      backgroundColor: Colors.red,
                    ),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextField(
                        controller: TextEditingController(text: memoria.nome),
                        decoration: InputDecoration(hintText: 'Nome'),
                        onChanged: (text) {
                          memoria.nome = text;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextField(
                        controller: TextEditingController(
                            text: memIntToString(memoria.addrInicio, aux)),
                        decoration: InputDecoration(hintText: 'Addr Início'),
                        onChanged: (text) {
                          memoria.addrInicio = int.parse(text);
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: TextField(
                        controller: TextEditingController(
                            text: memIntToString(memoria.tamanho, aux)),
                        decoration: InputDecoration(hintText: 'Tamanho'),
                        onChanged: (text) {
                          memoria.tamanho = int.parse(text);
                        },
                      ),
                    ),
                    RaisedButton(
                      elevation: 3.0,
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              titlePadding: const EdgeInsets.all(0.0),
                              contentPadding: const EdgeInsets.all(0.0),
                              content: SingleChildScrollView(
                                child: ColorPicker(
                                  pickerColor: memoria.cor,
                                  onColorChanged: (cor) =>
                                      {memoria.cor = cor}, //changeColor,
                                  colorPickerWidth: 200.0,
                                  pickerAreaHeightPercent: 0.7,
                                  enableAlpha: false,
                                  displayThumbColor: true,
                                  showLabel: true,
                                  paletteType: PaletteType.hsl,
                                  pickerAreaBorderRadius:
                                      const BorderRadius.only(
                                    topLeft: const Radius.circular(2.0),
                                    topRight: const Radius.circular(2.0),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                      child: const Text('Cor'),
                      /* color: memoria.cor,
                      textColor: useWhiteForeground(memoria.cor)
                          ? const Color(0xffffffff)
                          : const Color(0xff000000),*/
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        child: Text(textoAdicionarEditar(aux)),
                        onPressed: () {
                          setState(() {
                            listaMemorias.remove(mem_old);
                            var error = verifica(memoria);
                            if (error == 1) {
                              listaMemorias.add(new AreaMemoria(
                                  memoria.nome,
                                  memoria.addrInicio,
                                  memoria.tamanho,
                                  memoria.cor));
                              listaMemorias.sort((a, b) =>
                                  a.addrInicio.compareTo(b.addrInicio));
                              montaListaPontos();
                              Navigator.of(context).pop();
                            } else {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    titlePadding: const EdgeInsets.all(0.0),
                                    contentPadding: const EdgeInsets.all(0.0),
                                    content: error == 2
                                        ? Text("Espaço de memória já ocupado")
                                        : Text(
                                            "Valor total maior que tamanho da Memória"),
                                  );
                                },
                              );
                            }
                          });
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    //this._counter = 0;

//    listaMemorias.add(new AreaMemoria('123', 2, 5, Colors.blue));
//    listaMemorias.add(new AreaMemoria('asd', 10, 5, Colors.red));
//    listaMemorias.add(new AreaMemoria('123', 10, 5, Colors.red));

    return Scaffold(
      appBar: AppBar(
          title: Text(widget.title),
          actions: ([
            IconButton(
                icon: Icon(Icons.send),
                onPressed: () async {
                  await takeScreenShot();
                  print(diretorio);
                  Share.shareFiles(['$diretorio/screenshot.png'],
                      text: 'Mapa de memória');
                  //Share.share("imagem", subject: "Mapa de memória");
                })
          ])),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Tamanho memória"),
                ),
                Flexible(
                  fit: FlexFit.tight,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: SizedBox(
                      width: 250,
                      child: TextField(
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Tamanho",
                              isDense: true,
                              enabled: true),
                          onChanged: (text) {
                            tamanhoMapa = int.parse(text);
                          }),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Lista memorias:",
                  ),
                ),
                Column(
                  //children: listaMemorias.map((memoria) {
                  children: listaMemorias.map((memoria) {
                    return Card(
                      color: memoria.cor,
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        memoria.nome,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                          "Addr: ${memoria.addrInicio} Lenght: ${memoria.tamanho}"),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            editar_visivel == true
                                ? Column(
                                    children: [
                                      RaisedButton(
                                        child: Text("Remover"),
                                        onPressed: () {
                                          setState(() {
                                            listaMemorias.remove(memoria);
                                            print("Removendo: ");
                                            print(memoria);
                                            montaListaPontos();
                                          });
                                        },
                                      ),
                                      RaisedButton(
                                        child: Text("Editar"),
                                        onPressed: () {
                                          setState(() {
                                            novaMemoria(memoria);
                                          });
                                        },
                                      ),
                                    ],
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                ),
                Row(children: [
                  Container(
                      margin: const EdgeInsets.only(left: 2.0, right: 2.0),
                      child: FloatingActionButton(
                        onPressed: () {
                          novaMemoria(null);
                        },
                        tooltip: 'Adicionar',
                        child: Icon(Icons.add),
                      )),
                  Container(
                      margin: const EdgeInsets.only(left: 2.0, right: 2.0),
                      child: FloatingActionButton(
                        onPressed: () {
                          setState(() {
                            editar_visivel = !editar_visivel;
                          });
                        },
                        tooltip: 'Ocultar',
                        child: Icon(Icons.edit),
                      )),
                  Container(
                      margin: const EdgeInsets.only(left: 2.0, right: 2.0),
                      child: FloatingActionButton(
                        onPressed: () {
                          setState(() {
                            //listaMemorias = importJson(context);
                            _selectFile(context);
                          });
                        },
                        tooltip: 'Importar',
                        child: Icon(Icons.insert_drive_file_outlined),
                      )),
                  Container(
                      margin: const EdgeInsets.only(left: 2.0, right: 2.0),
                      child: FloatingActionButton(
                        onPressed: () {
                          makeJson(listaMemorias);
                        },
                        tooltip: 'Exportar',
                        child: Icon(Icons.save),
                      ))
                ]),
              ],
            ),
            RepaintBoundary(
              key: previewContainer,
              child: Column(
                children: [
                  Text("mapa"),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: SizedBox(
                        width: 500,
                        child: GridView.count(
                          primary: false,
                          padding: const EdgeInsets.all(20),
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          crossAxisCount: 16,
                          children: listaPontos.map((ponto) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.blueAccent),
                                color: ponto.cor,
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void montaListaPontos() {
    listaPontos = List.filled(tamanhoMapa, Ponto(0, Colors.white));

    for (var mapa in listaMemorias) {
      for (var i = mapa.addrInicio; i < (mapa.addrInicio + mapa.tamanho); i++) {
        //print(i);
        listaPontos[i] = Ponto(1, mapa.cor);
      }
    }
  }

  int verifica(AreaMemoria memoria) {
    //int retorno = 1;
    for (var mapa in listaMemorias) {
      if (((memoria.addrInicio >= mapa.addrInicio) &&
          (memoria.addrInicio < (mapa.addrInicio + mapa.tamanho)))) {
        return 2;
      } else if (memoria.addrInicio + memoria.tamanho > tamanhoMapa) {
        return 3;
      } else if ((mapa.addrInicio >= memoria.addrInicio) &&
          (mapa.addrInicio < (memoria.addrInicio + memoria.tamanho))) {
        return 2;
      } else {
        //retorno = 1;
      }
    }
    return 1;
  }

  String memIntToString(int valor, bool aux) {
    if (aux) {
      return valor.toString();
    } else {
      return null;
    }
  }

  String textoAdicionarEditar(bool aux) {
    if (aux) {
      return "Editar";
    } else {
      return "Adicionar";
    }
  }

  takeScreenShot() async {
    RenderRepaintBoundary boundary =
        previewContainer.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    //print(pngBytes);
    if (kIsWeb) {
      //final bytes = utf8.encode(encoded);
      final blob = html.Blob([pngBytes]);
      final url = html.Url.createObjectUrlFromBlob(blob);
      final anchor = html.document.createElement('a') as html.AnchorElement
        ..href = url
        ..style.display = 'none'
        ..download = 'screenshot.png';
      html.document.body.children.add(anchor);

// download
      anchor.click();

// cleanup
      html.document.body.children.remove(anchor);
      html.Url.revokeObjectUrl(url);
    } else {
      final directory = (await getApplicationDocumentsDirectory()).path;
      diretorio = directory;
      File imgFile = new File('$directory/screenshot.png');
      imgFile.writeAsBytes(pngBytes);
    }
  }

  List<AreaMemoria> importJson(context) {
    _selectFile(context);
    //var json = jsonDecode(data);
    //String arquivo = '[{"nome": "Teste", "addrInicio": 0, "tamanho": 0}]';

    //web
    List<AreaMemoria> lista = List();
    /*(json.decode(arquivo) as List)
        .map((data) => AreaMemoria.fromJson(data))
        .toList();*/
    //print(lista);
    return lista;
  }

  void _selectFile(context) {
    FilePickerCross.importMultipleFromStorage().then((filePicker) {
      setFilePicker(filePicker[0]);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('You selected ${filePicker.length} file(s).'),
        ),
      );
    });
  }

  setFilePicker(FilePickerCross filePicker) => setState(() {
        filePickerCross = filePicker;
        filePickerCross.saveToPath(path: filePickerCross.fileName);
        FilePickerCross.quota().then((value) {
          setState(() => quota = value);
        });
        //_fileString = filePickerCross.fileName.toString();
        //filePickerCross.
        try {
          _fileString = filePickerCross.toString();
        } catch (e) {
          _fileString = 'Not a text file. Showing base64.\n\n' +
              filePickerCross.toBase64();
        }
        print(_fileString);

        List<AreaMemoria> lista = (json.decode(_fileString) as List)
            .map((data) => AreaMemoria.fromJson(data))
            .toList();
        print(lista);
        listaMemorias = lista;
        montaListaPontos();
      });

  makeJson(List<AreaMemoria> lista) async {
    List jsonList = lista.map((variavel) => variavel.toJson()).toList();
    //print("jsonList: ${jsonList}");
    final encoded = jsonList.toString();
    // prepare

    final bytes = utf8.encode(encoded);
    final blob = html.Blob([bytes]);
    final url = html.Url.createObjectUrlFromBlob(blob);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = 'mapa.json';
    html.document.body.children.add(anchor);

// download
    anchor.click();

// cleanup
    html.document.body.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
  }
}
